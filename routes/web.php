<?php

use App\Http\Controllers\CourseController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/role', function () {
//     return view('backend.roles.role');
// });

//for role
Route::get('/roleIndex',[RoleController::class, 'roleIndex'])->name('roleIndex');
Route::get('/roleCreate',[RoleController::class, 'roleCreate'])->name('roleCreate');
Route::post('/roleStore',[RoleController::class, 'roleStore'])->name('roleStore');
Route::get('/roleShow/{role}',[RoleController::class, 'roleShow'])->name('roleShow');
Route::get('/roleEdit/{role}',[RoleController::class, 'roleEdit'])->name('roleEdit');
Route::patch('/roleEditStore/{role}',[RoleController::class, 'roleEditStore'])->name('roleEditStore');
Route::delete('/roleDelete/{role}',[RoleController::class, 'roleDelete'])->name('roleDelete');

//for userREgistration

Route::get('/userIndex',[StudentController::class, 'userIndex'])->name('userIndex');
Route::get('/userCreate',[StudentController::class, 'userCreate'])->name('userCreate');
Route::post('/userStore',[StudentController::class, 'userStore'])->name('userStore');
Route::get('/userShow/{user}',[StudentController::class, 'userShow'])->name('userShow');
Route::get('/userEdit/{user}',[StudentController::class, 'userEdit'])->name('userEdit');
Route::patch('/userEditStore/{user}',[StudentController::class, 'userEditStore'])->name('userEditStore');
Route::delete('/userDelete/{user}',[StudentController::class, 'userDelete'])->name('userDelete');

//for course
Route::get('/courseIndex',[CourseController::class,'courseIndex'])->name('courseIndex');
Route::get('/courseCreate',[CourseController::class,'courseCreate'])->name('courseCreate');
Route::post('/courseStore',[CourseController::class,'courseStore'])->name('courseStore');
Route::get('/courseShow/{course}',[CourseController::class,'courseShow'])->name('courseShow');
Route::get('/courseEdit/{course}',[CourseController::class,'courseEdit'])->name('courseEdit');
Route::patch('/courseEditStore/{course}',[CourseController::class,'courseEditStore'])->name('courseEditStore');
Route::delete('/courseDelete/{course}',[CourseController::class,'courseDelete'])->name('courseDelete');