<x-backend.layouts.master>
    <form action="{{ route('userStore') }}" method="POST" enctype="multipart/form-data">
        @CSRF
        <div class="mb-1">
            <label for="name" class="form-label">Name</label>
            <input type="text" name="name" class="form-control w-25" id="name"
                aria-describedby="emailHelp">
        </div>
        <div class="mb-2">
            <label for="email" class="form-label">Email</label>
            <input type="email" name="email" class="form-control w-25" id="email">
        </div>
        <div class="mb-2">
            <label for="phone" class="form-label">Phone Number</label>
            <input type="text" name="phone" class="form-control w-25" id="phone">
        </div>
        <div class="mb-2">
            <label for="dob" class="form-label">DOB</label></label>
            <input type="date" name="dob" class="form-control w-25" id="dob">
        </div>
        <div class="mb-1">
            <label for="gender">Gender</label><br>
            <input type="radio" id="male" name="gender" value="male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="female">
            <label for="female">Female</label><br>
            <input type="radio" id="female" name="gender" value="others">
            <label for="female">Others</label><br>
        </div>
        <div class="mb-2">
            <select name="roles_id" id="roles_id" class="form-select form-control w-25" aria-label="Default select example">
                <option selected>---Registration As---</option>
                @foreach ($roles as $key => $role)
                    <option value="{{ $key }}">{{ $role }}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-2">
            <label for="password" class="form-label">password</label></label>
            <input type="password" name="password" class="form-control w-25" id="password">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</x-backend.layouts.master>
