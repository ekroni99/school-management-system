<x-backend.layouts.master>
    <h1>Name : {{ $user->name }}</h1>
    <p>Email : {{ $user->email }}</p>
    <p>Phone : {{ $user->phpne }}</p>
    <p>DOB : {{ $user->DOB }}</p>
    <p>Gender : {{ $user->gender }}</p>
    <p>Role Name : {{ $user->role->roles_name }}</p>
</x-backend.layouts.master>