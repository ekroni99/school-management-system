<x-backend.layouts.master>
    <table style="width: 100%; border-collapse: collapse" border="1">
        <thead>
            <tr>
                <th colspan="8">
                    <h3 class="text-center">User</h3>
                </th>
            </tr>
            <tr>
                <th colspan="8" style="text-align: right">
                    <a class="btn btn-primary btn-sm" href="{{ route('userCreate') }}">Add New User</a>
                </th>
            </tr>
            <tr>
                <th>Sno</th>
                <th>User Name</th>
                <th>User Email</th>
                <th>User Phone</th>
                <th>User DOB</th>
                <th>User Gender</th>
                <th>User Role</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @php
                $sl = 1;
            @endphp
            @foreach ($students as $student)
                <tr>
                    <td>{{ $sl++ }}</td>
                    <td>{{ $student->name }}</td>
                    <td>{{ $student->email }}</td>
                    <td>{{ $student->phone }}</td>
                    <td>{{ $student->DOB }}</td>
                    <td>{{ $student->gender }}</td>
                    <td>{{ $student->role->roles_name }}</td>
                    <td class="text-center">
                        <a class="btn btn-success btn-sm" href="{{ route('userShow',$student->id) }}">Show</a>
                        <a class="btn btn-primary btn-sm" href="{{ route('userEdit',$student->id) }}">Edit</a>
                        <form class="d-inline" action="{{route('userDelete', $student->id)}}" method="post">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</x-backend.layouts.master>
