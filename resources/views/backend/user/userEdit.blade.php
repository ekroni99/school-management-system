<x-backend.layouts.master>
    <form action="{{ route('userEditStore',$user->id) }}" method="POST" enctype="multipart/form-data">
        @CSRF
        @method('PATCH')
        <div class="mb-1">
            <label for="name" class="form-label">Name</label>
            <input type="text" value="{{ $user->name }}" name="name" class="form-control w-25" id="name"
                aria-describedby="emailHelp">
        </div>
        <div class="mb-2">
            <label for="email" class="form-label">Email</label>
            <input type="email" value="{{ $user->email }}" name="email" class="form-control w-25" id="email">
        </div>
        <div class="mb-2">
            <label for="phone" class="form-label">Phone Number</label>
            <input type="text" value="{{ $user->phone }}" name="phone" class="form-control w-25" id="phone">
        </div>
        <div class="mb-2">
            <label for="dob" class="form-label">DOB</label></label>
            <input type="date" value="{{ $user->DOB }}" name="dob" class="form-control w-25" id="dob">
        </div>
        <div class="mb-1">
            <label for="gender">Gender</label><br>
            <input type="radio" id="male" name="gender" value="male" @if ($user->gender==='male') checked @endif>
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="female" @if ($user->gender==='female') checked  @endif>
            <label for="female">Female</label><br>
            <input type="radio" id="female" name="gender" value="others" @if ($user->gender==="others") checked @endif>
            <label for="female">Others</label><br>
        </div>
        <div class="mb-2">
            <select name="roles_id" id="roles_id" class="form-select form-control w-25" aria-label="Default select example">
                <option selected>---Updated As---</option>
                @foreach ($roles as $key => $role)
                    <option value="{{ $key }}">{{ $role }}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-2">
            <label for="password" class="form-label">password</label></label>
            <input type="password" value="{{ $user->password }}" name="password" class="form-control w-25" id="password">
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>

</x-backend.layouts.master>