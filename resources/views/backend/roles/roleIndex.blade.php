<x-backend.layouts.master>
    <table style="width: 100%; border-collapse: collapse" border="1">
        <thead>
            <tr>
                <th colspan="8">
                    <h3 class="text-center">Role</h3>
                </th>
            </tr>
            <tr>
                <th colspan="8" style="text-align: right">
                    <a class="btn btn-primary btn-sm" href="{{ route('roleCreate') }}">Add New Role</a>
                </th>
            </tr>
            <tr>
                <th>Sno</th>
                <th>Role Name</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @php
                $sl = 1;
            @endphp
            @foreach ($roles as $key => $role)
                <tr>
                    <td>{{ $sl++ }}</td>
                    <td>{{ $role->roles_name }}</td>
                    <td class="text-center">
                        <a class="btn btn-success btn-sm" href="{{ route('roleShow',$role->id) }}">Show</a>
                        <a class="btn btn-primary btn-sm" href="{{ route('roleEdit',$role->id) }}">Edit</a>
                        <form class="d-inline" action="{{route('roleDelete', $role->id)}}" method="post">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</x-backend.layouts.master>
