<x-backend.layouts.master>
    <form action="{{route('roleEditStore',$role->id)}}" method="POST">
        @csrf
        @method('PATCH')
        <div class="mb-3">
            <label for="role" class="form-label"> Create a role</label>
            <input type="text" value="{{ $role->roles_name }}" class="form-control w-25" id="role" name="role_name" placeholder="role name">
        </div>
        <button type="submit" class="btn btn-primary btn-sm">Update</button>
    </form>
</x-backend.layouts.master>