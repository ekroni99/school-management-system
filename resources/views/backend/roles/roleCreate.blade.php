<x-backend.layouts.master>
    <form action="{{route('roleStore')}}" method="POST">
        @csrf
        <div class="mb-3">
            <label for="role" class="form-label"> Create a role</label>
            <input type="text" class="form-control w-25" id="role" name="role_name" placeholder="role name">
        </div>
        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
    </form>
</x-backend.layouts.master>
