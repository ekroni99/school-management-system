<x-backend.layouts.master>
    <form action="{{ route('courseStore') }}" method="POST" enctype="multipart/form-data">
        @CSRF
        <div class="mb-1">
            <label for="name" class="form-label">Create Course Name</label>
            <input type="text" name="course_name" class="form-control w-25" id="name" aria-describedby="emailHelp" placeholder="Course Name ">
        </div>
        <div class="mb-2">
            <select name="student_id" id="student_id" class="form-select form-control w-25"
                aria-label="Default select example">
                <option selected>---Add Student---</option>
                @foreach ($students as $key => $student)
                    <option value="{{ $student->id }}">{{ $student->name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</x-backend.layouts.master>
