<x-backend.layouts.master>
    <h1>Course Name : {{ $course->course_name }}</h1>
    <h2>Student Name : {{ $course->student->name }}</h2>
    <h3>Role : {{ $course->student->role->roles_name }}</h3>
</x-backend.layouts.master>