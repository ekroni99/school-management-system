<x-backend.layouts.master>
     <table style="width: 100%; border-collapse: collapse" border="1">
        <thead>
            <tr>
                <th colspan="8">
                    <h3 class="text-center">Course</h3>
                </th>
            </tr>
            <tr>
                <th colspan="8" style="text-align: right">
                    <a class="btn btn-primary btn-sm" href="{{ route('courseCreate') }}">Add New Course</a>
                </th>
            </tr>
            <tr>
                <th>Sno</th>
                <th>Course Name</th>
                <th>Student Name</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @php
                $sl = 1;
            @endphp
            @foreach ($courses as $key => $course)
                <tr>
                    <td>{{ $sl++ }}</td>
                    <td>{{ $course->course_name }}</td>
                    <td>{{ $course->student->name }}</td>
                    <td class="text-center">
                        <a class="btn btn-success btn-sm" href="{{ route('courseShow',$course->id) }}">Show</a>
                        <a class="btn btn-primary btn-sm" href="{{ route('courseEdit',$course->id) }}">Edit</a>
                        <form class="d-inline" action="{{route('courseDelete', $course->id)}}" method="post">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</x-backend.layouts.master>