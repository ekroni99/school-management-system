<!DOCTYPE html>
<html lang="en">
<x-backend.layouts.partials.header />

<body>
    <!-- Main navbar -->
    <x-backend.layouts.partials.navbar />
    <!-- /main navbar -->


    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <x-backend.layouts.partials.sidebar />
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">
            <div class="content">
                <!-- Page header -->
                {{-- <x-backend.layouts.partials.pageheader /> --}}
                <!-- /page header -->


                <!-- Content area -->
                {{ $slot }}
                <!-- /content area -->

            </div>
            <!-- Footer -->
            @php
                $roni = 'i am ekRoni';
            @endphp
            <x-backend.layouts.partials.footer :roni="$roni" />
            <!-- /footer -->
        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
</body>

</html>
