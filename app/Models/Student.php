<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Student extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }
    public function course(): HasMany
    {
        return $this->hasMany(Course::class);
    }
}
