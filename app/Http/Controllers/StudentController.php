<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Student;
use Illuminate\Http\Request;


class StudentController extends Controller
{
    public function userIndex()
    {
        $students = Student::all();
        return view('backend.user.userIndex',compact('students'));
    }
    public function userCreate()
    {
        $roles=Role::orderBy('roles_name','asc')
             ->pluck('roles_name', 'id')
            ->toArray();

        return view('backend.user.userCreate', compact('roles'));
       
    }
    public function userStore(Request $request){
       $student=new Student();
       $student->name=$request->name;
       $student->email=$request->email;
       $student->phone=$request->phone;
       $student->dob=$request->dob;
       $student->gender=$request->gender;
       $student->role_id=$request->roles_id;
       $student->password=$request->password;
       $student->save();
       return redirect()->route('userIndex');
    }
    public function userShow(Student $user){
        
        return view('backend.user.userShow',compact('user'));
    }
    public function userEdit(Student $user){
        $roles=Role::orderBy('roles_name','asc')
        ->pluck('roles_name', 'id')
       ->toArray();
        return view('backend.user.userEdit',compact('user','roles'));
    }
    public function userEditStore(Request $request, Student $user){
        $user->update([
        'name'=>$request->name,
        'email'=>$request->email,
        'phone'=>$request->phone,
        'dob'=>$request->dob,
        'gender'=>$request->gender,
        'role_id'=>$request->roles_id,
        'password'=>$request->password
        ]);
        return redirect()->route('userIndex');
    }
    public function userDelete(Student $user){
        $user->delete();
        return redirect()->route('userIndex');
    }
}
