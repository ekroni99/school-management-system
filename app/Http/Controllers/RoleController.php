<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function roleIndex(Request $request)
    {
        $roles = Role::all();
        return view('backend.roles.roleIndex', compact('roles'));
    }
    public function roleCreate(Request $request)
    {
        return view('backend.roles.roleCreate');
    }
    public function roleStore(Request $request)
    {
        $role = new Role();
        $role->roles_name = $request->role_name;
        $role->save();
        return redirect()->route('roleIndex');
    }

    public function roleShow(Role $role){
        return view('backend.roles.roleShow',compact('role'));
    }
    public function roleEdit(Role $role){
        return view('backend.roles.roleEdit',compact('role'));
    }
    public function roleEditStore(Request $request, Role $role){
        $role->update([
            'roles_name'=>$request->role_name
        ]);
        return redirect()->route('roleIndex');
    }
    public function roleDelete(Request $request,Role $role){
        $role->delete();
        return redirect()->route('roleIndex');
    }
}
