<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Role;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CourseController extends Controller
{
    public function courseIndex(){
        $courses=Course::all();
        return view('backend.course.courseIndex',compact('courses'));
    }
    public function courseCreate(){
        $students = Student::where('role_id', 1)->get();
        return view('backend.course.courseCreate',compact('students'));
    }
    public function courseStore(Request $request){
        $course=new Course();
        $course->course_name=$request->course_name;
        $course->student_id=$request->student_id;
        $course->save();
        return redirect()->route('courseIndex');
    }
    public function courseShow(Course $course){
        return view('backend.course.courseShow',compact('course'));
    }
    public function courseEdit(Course $course){
        $students = Student::where('role_id', 1)->get();
        return view('backend.course.courseEdit',compact('course','students'));
    }
    public function courseEditStore(Request $request,Course $course){
        $course->update([
            'course_name'=>$request->course_name,
        ]);
        return redirect()->route('courseIndex');
    }
    public function courseDelete(Course $course){
        $course->delete();
        return redirect()->route('courseIndex');
    }
}
